import React, { ReactElement } from 'react'
import Button, { ButtonProps } from '../Atoms/Button'
import { styled } from '../../theme'

const List = styled.div`
  width: 390px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export interface CommandProps {
  actions: { icon?: ButtonProps['icon']; onPress: void | null }[]
}

export default function Command({ actions }: CommandProps): ReactElement {
  return (
    <List>
      {actions.map(({ icon, onPress }, index) => (
        <Button key={index.toString()} icon={icon} circle label={icon} onClick={() => onPress} />
      ))}
    </List>
  )
}
