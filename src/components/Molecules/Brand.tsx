import React, { ReactElement } from 'react'
import Typography from '../Atoms/Typography'
import { styled } from '../../theme'
import Logo from '../Atoms/Logo'

const { Text, Title } = Typography

const Wrapper = styled.div`
  display: flex;
`

const List = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 25px;
  @media (max-width: 768px) {
    display: none;
  }
`

export interface BrandProps {
  title: string
  subtitle: string
}

export default function Brand({ title, subtitle }: BrandProps): ReactElement {
  return (
    <Wrapper>
      <Logo />
      <List>
        <Title>{title}</Title>
        <Text fade>{subtitle}</Text>
      </List>
    </Wrapper>
  )
}
