import React, { ReactElement } from 'react'
import { styled } from '../../theme'
import Typography from '../Atoms/Typography'
import Icon, { iconProps } from '../Atoms/Icon'

const { Text } = Typography

const Wrapper = styled.div`
  border-top: 1px solid ${(p) => p.theme.color.darkFaded};
  margin-left: -24px;
  margin-bottom: -16px;
  margin-top: 34px;
  padding: 16px 24px;
  width: calc(100%);
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`

const Content = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  padding-bottom: 16px;
`

export type ComposeProps = {
  placeholder: string
  icons: iconProps[]
}

export default function Compose(props: ComposeProps): ReactElement {
  const { icons, placeholder } = props
  return (
    <Wrapper>
      <Content>
        {icons.map((name, index) => (
          <Icon key={index.toString()} name={name} />
        ))}
      </Content>
      <Text fade>{placeholder}</Text>
    </Wrapper>
  )
}
