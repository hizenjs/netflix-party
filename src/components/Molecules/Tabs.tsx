import React, { ReactElement } from 'react'
import Button, { ButtonProps } from '../Atoms/Button'
import { styled } from '../../theme'
import Icon from '../Atoms/Icon'
import Typography from '../Atoms/Typography'

const { Link } = Typography

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const List = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`

export interface TabsProps {
  tabs: { onPress: void | null; name?: string; active?: boolean | false }[]
}

export default function Tabs({ tabs }: TabsProps): ReactElement {
  return (
    <Wrapper>
      <List>
        {tabs.map(({ name, active }, index) => (
          <Link active={active} bold key={index.toString()}>
            {name}
          </Link>
        ))}
      </List>
      <Icon name={'maximize'} />
    </Wrapper>
  )
}
