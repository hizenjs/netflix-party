import React, { ReactElement, useContext, useRef, useState } from 'react'
import { styled } from '../../theme'
import Icon from '../Atoms/Icon'
import Progress from '../Atoms/Progress'
import Typography from '../Atoms/Typography'
import { AppContext } from '../../provider'

const { Text } = Typography

const Wrapper = styled.div`
  margin-top: 26px;
  width: calc(100% - (18px * 2));
  padding: 15px 18px;
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  border-radius: 12px;
  background: ${(props) => props.theme.color.lightAccent}0C;
`

const Head = styled.div`
  display: flex;
  justify-content: space-between;
`

const Option = styled.div`
  padding-top: 16px;
`
const Spacer = styled.div`
  padding-top: 4px;
`

export type PollProps = {
  title: string
  options: { value: number; name: string }[]
}

export default function Poll(props: PollProps): ReactElement {
  const { showPoll, setShowPoll } = useContext(AppContext)
  const { title, options } = props

  return (
    <>
      {showPoll && (
        <Wrapper>
          <Head>
            <Text fade>{title}</Text>
            <Icon onClick={() => setShowPoll(false)} wrap name={'close'} />
          </Head>
          {options.map(({ value, name }, index) => (
            <Option key={index.toString()}>
              <Progress value={value} />
              <Spacer />
              <Text fade>{`${value}% ${name}`}</Text>
            </Option>
          ))}
        </Wrapper>
      )}
    </>
  )
}
