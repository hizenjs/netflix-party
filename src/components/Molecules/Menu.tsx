import React, { ReactElement } from 'react'
import Button, { ButtonProps } from '../Atoms/Button'
import { styled } from '../../theme'

const List = styled.div`
  width: 310px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export interface MenuProps {
  actions: { icon?: ButtonProps['icon']; onPress: void | null; name?: string }[]
}

export default function Menu({ actions }: MenuProps): ReactElement {
  return (
    <List>
      {actions.map(({ name, icon }, index) =>
        name ? <Button key={index.toString()}>{name}</Button> : <Button key={index.toString()} icon={icon} />,
      )}
    </List>
  )
}
