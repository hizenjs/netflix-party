import React, { ReactElement } from 'react'
import { styled } from '../../theme'
import Avatar, { ImageProps } from '../Atoms/Avatar'
import Typography from '../Atoms/Typography'
import Message from '../Atoms/Message'

const { Text } = Typography

const Wrapper = styled.div`
  margin-top: 26px;
  width: 75%;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
`

const Content = styled.div`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

const Spacer = styled.div`
  padding-top: 4px;
`

export type ConversationProps = {
  name: string
  messages: React.ReactText[]
  avatar: ImageProps
}

export default function Conversation(props: ConversationProps): ReactElement {
  const { name, messages, avatar } = props
  return (
    <Wrapper>
      <Avatar {...avatar} />
      <Content>
        <Text small fade>
          {name}
        </Text>
        <Spacer />
        {messages.map((text, index) => (
          <>
            <Spacer />
            <Message key={index.toString()}>{text}</Message>
          </>
        ))}
      </Content>
    </Wrapper>
  )
}
