import React, { ReactElement } from 'react'
import { styled } from '../../theme'
import Header, { HeaderProps } from '../Organism/Header'
import Video, { VideoProps } from '../Organism/Video'
import Chat, { ChatProps } from '../Organism/Chat'

interface MainProps {
  header: HeaderProps
  video: VideoProps
  chat: ChatProps
}

const Wrapper = styled.div`
  width: calc(100vw - (26px * 2));
  height: calc(100vh - (26px * 2));
  padding: 26px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: ${(props) => props.theme.color.darkAccent};
  @media (max-width: 1048px) {
    flex-direction: column;
  }
`

const Container = styled.div`
  width: 73.515625%;
  margin-right: 25px;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  background: ${(props) => props.theme.color.darkAccent};
  @media (max-width: 1048px) {
    width: 100%;
    margin-right: 0;
  }
`

export default function Main(props: MainProps): ReactElement {
  return (
    <Wrapper>
      <Container>
        <Header {...props.header} />
        <Video {...props.video} />
      </Container>
      <Chat {...props.chat} />
    </Wrapper>
  )
}
