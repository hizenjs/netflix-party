import Avatar from './Atoms/Avatar'
import Button from './Atoms/Button'
import Icon from './Atoms/Icon'
import Logo from './Atoms/Logo'
import Message from './Atoms/Message'
import Player from './Atoms/Player'
import Progress from './Atoms/Progress'
import Typography from './Atoms/Typography'

import Brand from './Molecules/Brand'
import Command from './Molecules/Command'
import Menu from './Molecules/Menu'

import Chat from './Organism/Chat'
import Header from './Organism/Header'
import Video from './Organism/Video'

import Main from './Templates/Main'

const atoms = { Typography, Avatar, Progress, Button, Message, Logo, Icon, Player }
const molecules = { Menu, Brand, Command }
const organism = { Header, Video, Chat }
const templates = { Main }

export { atoms, molecules, organism, templates }
