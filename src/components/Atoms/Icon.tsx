import React, { ReactElement } from 'react'
import { FiX, FiAtSign, FiImage, FiPaperclip, FiMaximize } from 'react-icons/fi'
import { styled } from '../../theme'

export interface IconProps {
  name: iconProps
  wrap?: boolean
  onClick?: () => void
}

export type iconProps = 'close' | 'mention' | 'image' | 'join' | 'maximize'

const ico: { [key: string]: ReactElement<iconProps> } = {
  close: <FiX />,
  mention: <FiAtSign />,
  image: <FiImage />,
  join: <FiPaperclip />,
  maximize: <FiMaximize />,
}

const Content = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  text-align: center;
  color: ${(p) => p.theme.color.alternative};
  background: none;
  border: none;
  cursor: pointer;
  transition: 300ms;
  :hover {
    color: ${(p) => p.theme.color.lightAccent};
  }
  :focus {
    outline: none;
  }
`

const Wrapper = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 30px;
  height: 30px;
  font-size: 30px;
  text-align: center;
  color: ${(p) => p.theme.color.lightFaded};
  background: ${(p) => p.theme.color.darkFaded};
  border: none;
  border-radius: 26px;
  cursor: pointer;
  transition: 300ms;
  :hover {
    color: ${(p) => p.theme.color.lightAccent};
  }
  :focus {
    outline: none;
  }
`

export const Icon = ({ onClick, wrap, name }: IconProps): ReactElement =>
  (wrap && (
    <Wrapper onClick={onClick} style={{ fontSize: 20 }}>
      {ico[name]}
    </Wrapper>
  )) || <Content>{ico[name]}</Content>
export default Icon
