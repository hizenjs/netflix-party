import React, { ReactElement } from 'react'
import { styled } from '../../theme'

interface ProgressProps {
  value?: number | 0
}

const ProgressOuter = styled.div`
  width: 100%;
  height: 4px;
  border-radius: 4px;
  background: ${(p) => p.theme.color.alternative};
`

const ProgressInner = styled.div`
  height: 4px;
  width: ${(p: ProgressProps) => p.value}%;
  border-radius: 4px;
  background: ${(p) => p.theme.color.lightAccent};
  transition: 300ms;
`

export const Progress = (props: ProgressProps): ReactElement => {
  return (
    <ProgressOuter>
      <ProgressInner {...props} />
    </ProgressOuter>
  )
}
export default Progress
