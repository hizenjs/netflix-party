import React, { ReactElement } from 'react'
import { styled } from '../../theme'

export interface PlayerProps {
  source?: string
  stream?: string | undefined
  maximized?: boolean
  loading?: boolean
}

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  margin-bottom: 24px;
`

const AspectRatio = styled.figure`
  width: 100%;
  overflow: hidden;
  margin: 0;
  padding-top: 56.25%;
  position: relative;
  background: black;
  border-radius: ${(p: PlayerProps) => !p.maximized && 18}px;
`

const Image = styled.img`
  position: absolute;
  object-fit: cover;
  max-height: 710px;
  top: 50%;
  left: 50%;
  width: 100%;
  transform: translate(-50%, -50%);
  border-radius: ${(p: PlayerProps) => !p.maximized && 18}px;
`

export const Player = ({ source }: PlayerProps): ReactElement => (
  <Container>
    <AspectRatio>
      <Image src={source} />
    </AspectRatio>
  </Container>
)
export default Player
