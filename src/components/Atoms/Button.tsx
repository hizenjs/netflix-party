import React, { ReactElement } from 'react'
import {
  FiShare,
  FiSettings,
  FiMoreHorizontal,
  FiVideo,
  FiCast,
  FiRewind,
  FiFastForward,
  FiBarChart2,
  FiPlay,
} from 'react-icons/fi'
import { styled } from '../../theme'

export interface ButtonProps {
  children?: React.ReactText | ''
  icon?: iconProps
  circle?: boolean
  label?: undefined | string
  onClick?: () => void
}

type iconProps = 'share' | 'settings' | 'more' | 'video' | 'cast' | 'rewind' | 'forward' | 'chart' | 'play'

const ico: { [key: string]: ReactElement<iconProps> } = {
  share: <FiShare />,
  settings: <FiSettings />,
  more: <FiMoreHorizontal />,
  video: <FiVideo />,
  cast: <FiCast />,
  rewind: <FiRewind />,
  forward: <FiFastForward />,
  chart: <FiBarChart2 />,
  play: <FiPlay />,
}

const Text = styled.button`
  display: block;
  padding: 12px 18px;
  font-size: 14px;
  font-weight: 400;
  line-height: 16px;
  text-align: center;
  color: ${(p) => p.theme.color.lightFaded};
  background: ${(p) => p.theme.color.darkFaded};
  border: none;
  border-radius: 12px;
  cursor: pointer;
  transition: 300ms;
  :hover {
    color: ${(p) => p.theme.color.lightAccent};
  }
  :focus {
    outline: none;
  }
`

const Icon = styled.button`
  display: block;
  width: 40px;
  height: 40px;
  font-size: 16px;
  line-height: 22px;
  font-weight: 400;
  text-align: center;
  color: ${(p) => p.theme.color.lightFaded};
  background: ${(p) => p.theme.color.darkShade};
  border: none;
  cursor: pointer;
  transition: 300ms;
  :hover {
    color: ${(p) => p.theme.color.lightAccent};
  }
  :focus {
    outline: none;
  }
`

const Label = styled.p`
  text-align: center;
  text-transform: capitalize;
  color: ${(p) => p.theme.color.darkFaded};
  cursor: default;
  font-size: 14px;
  margin: 5px 0;
`
const Centered = styled.div`
  width: 50px;
  display: flex;
  align-items: center;
  flex-direction: column;
`

const CustomStyle = { borderRadius: 50, width: 50, height: 50, fontSize: 24 }

export const Button = (props: ButtonProps): ReactElement => {
  const { children, icon, circle, label, onClick } = props
  if (icon && label)
    return (
      <Centered onClick={() => onClick}>
        <Icon style={CustomStyle}>{ico[icon]}</Icon>
        <Label>{label}</Label>
      </Centered>
    )
  if (icon)
    return (
      <Icon onClick={() => onClick} style={circle ? { borderRadius: 40 } : { borderRadius: 12 }}>
        {ico[icon]}
      </Icon>
    )
  return <Text onClick={() => onClick}>{children}</Text>
}
export default Button
