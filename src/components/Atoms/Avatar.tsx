import React, { ReactElement } from 'react'
import { styled } from '../../theme'

export interface ImageProps {
  type?: 'circle' | 'rounded' | 'square'
  size?: 'small' | 'medium' | 'large'
  source?: string
  letter?: string
}

const size: { [key: string]: number } = { large: 54, medium: 40, small: 30 }
const radius: { [key: string]: number } = { circle: 28, rounded: 8, square: 0 }

const Image = styled.img`
  width: ${(p: ImageProps) => size[p.size || 'medium']}px;
  height: ${(p: ImageProps) => size[p.size || 'medium']}px;
  border-radius: ${(p: ImageProps) => radius[p.type || 'circle']}px;
  object-fit: cover;
`

const Letter = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${(p: ImageProps) => size[p.size || 'medium']}px;
  height: ${(p: ImageProps) => size[p.size || 'medium']}px;
  border-radius: ${(p: ImageProps) => radius[p.type || 'circle']}px;
  background: ${(p) => p.theme.color.lightAccent};
  color: ${(p) => p.theme.color.primary};
  text-align: center;
`

export const Avatar = (props: ImageProps): ReactElement => {
  const { letter, source } = props
  if (source) return <Image {...props} src={source} />
  return <Letter {...props}>{letter}</Letter>
}
export default Avatar
