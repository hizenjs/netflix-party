import React, { ReactElement } from 'react'
import { styled } from '../../theme'

export interface MessageProps {
  children?: React.ReactText | ''
}

const Text = styled.div`
  width: fit-content;
  padding: 5px 8px;
  font-size: 14px;
  font-weight: 400;
  line-height: 16px;
  color: ${(p) => p.theme.color.lightFaded};
  background: ${(p) => p.theme.color.darkFaded};
  border-radius: 8px;
  transition: 300ms;
`

export const Message = ({ children }: MessageProps): ReactElement => <Text>{children}</Text>
export default Message
