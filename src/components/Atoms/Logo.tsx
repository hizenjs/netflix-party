import React, { ReactElement } from 'react'
import original from '../../assets/logo.svg'
import small from '../../assets/logo_small.svg'
import app from '../../assets/logo_app.svg'
import { styled } from '../../theme'

interface BrandProps {
  version?: 'app' | 'small' | 'original'
}

const logo: { [key: string]: string } = { app, small, original }

const Svg = styled.img`
  max-width: 98px;
  height: 36px;
  object-fit: contain;
`

export const Logo = ({ version = 'original' }: BrandProps): ReactElement => <Svg src={logo[version]} />
export default Logo
