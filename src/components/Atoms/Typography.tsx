import { styled } from '../../theme'

interface TextProps {
  bold?: boolean
  fade?: boolean
  small?: boolean
  active?: boolean
}

type TitleProps = TextProps

const Text = styled('p')`
  margin: 0;
  font-size: ${(p: TextProps) => (p.small ? 12 : 14)}px;
  font-weight: ${(p: TitleProps) => (p.bold ? 600 : 400)};
  line-height: ${(p: TextProps) => (p.small ? 16 : 16)}px;
  opacity: ${(p: TextProps) => (p.fade ? 0.6 : 1)};
  color: ${(p) => p.theme.color.lightAccent};
`

const Title = styled('h1')`
  margin: 0;
  font-size: 20px;
  font-weight: ${(p: TitleProps) => (p.bold ? 600 : 400)};
  line-height: 24px;
  opacity: ${(p: TitleProps) => (p.fade ? 0.6 : 1)};
  color: ${(p) => p.theme.color.lightAccent};
`

const Link = styled('a')`
  margin: 0;
  padding-right: 18px;
  font-size: ${(p: TextProps) => (p.small ? 12 : 14)}px;
  font-weight: ${(p: TitleProps) => (p.bold ? 600 : 400)};
  line-height: ${(p: TextProps) => (p.small ? 16 : 16)}px;
  opacity: ${(p: TextProps) => (p.active ? 1 : 0.6)};
  color: ${(p) => p.theme.color.lightAccent};
  text-decoration: none;
  cursor: pointer;
`

const Typography = {
  Text,
  Title,
  Link,
}

export default Typography
