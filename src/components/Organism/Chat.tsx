import React, { ReactElement } from 'react'
import { styled } from '../../theme'
import Tabs, { TabsProps } from '../Molecules/Tabs'
import Poll, { PollProps } from '../Molecules/Poll'
import Conversation, { ConversationProps } from '../Molecules/Conversation'
import Compose, { ComposeProps } from '../Molecules/Compose'

const Wrapper = styled.div`
  width: calc(26.484375% - (24px * 2));
  max-width: calc(468px - (24px * 2));
  padding: 24px;
  height: calc(100% - (24px * 2));
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  flex-direction: column;
  border-radius: 18px;
  background: ${(props) => props.theme.color.darkShade};
  @media (max-width: 1048px) {
    width: calc(100% - (24px * 2));
    margin-top: 26px;
    max-width: 100%;
    margin-right: 0;
  }
`

export interface ChatProps extends TabsProps {
  poll: PollProps
  conversations: ConversationProps[]
  compose: ComposeProps
}

export default function Chat(props: ChatProps): ReactElement {
  const { tabs, poll, conversations, compose } = props
  return (
    <Wrapper>
      <Tabs tabs={tabs} />
      <Poll {...poll} />
      {conversations.map((conversation, index) => (
        <Conversation key={index.toString()} {...conversation} />
      ))}
      <Compose {...compose} />
    </Wrapper>
  )
}
