import React, { ReactElement } from 'react'
import Menu, { MenuProps } from '../Molecules/Menu'
import Brand, { BrandProps } from '../Molecules/Brand'
import { styled } from '../../theme'

export interface HeaderProps extends MenuProps, BrandProps {}

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: ${(props) => props.theme.color.darkAccent};
`

export default function Header(props: HeaderProps): ReactElement {
  const { title, actions, subtitle } = props
  return (
    <Wrapper>
      <Brand title={title} subtitle={subtitle} />
      <Menu actions={actions} />
    </Wrapper>
  )
}
