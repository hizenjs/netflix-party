import React, { ReactElement } from 'react'
import { styled } from '../../theme'
import Player, { PlayerProps } from '../Atoms/Player'
import Command, { CommandProps } from '../Molecules/Command'

export interface VideoProps extends PlayerProps, CommandProps {}

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  flex-direction: column;
  background: ${(props) => props.theme.color.darkAccent};
  @media (max-width: 1048px) {
    margin-top: 26px;
  }
`

export default function Header(props: VideoProps): ReactElement {
  const { actions } = props
  return (
    <Wrapper>
      <Player {...props} />
      <Command actions={actions} />
    </Wrapper>
  )
}
