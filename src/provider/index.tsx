import React, { createContext, useState } from 'react'

interface ProviderProps {
  children: React.ReactNode
}

type ContextProps = {
  showPoll: boolean
  setShowPoll: React.Dispatch<React.SetStateAction<boolean>>
}

const exception = new Error('function not implemented')

export const AppContext = createContext<ContextProps>({
  setShowPoll: () => {
    throw exception
  },
  showPoll: false,
})

const AppProvider: React.FC<ProviderProps> = ({ children }: ProviderProps) => {
  const [showPoll, setShowPoll] = useState<boolean>(true)

  return <AppContext.Provider value={{ showPoll, setShowPoll }}>{children}</AppContext.Provider>
}

export default AppProvider
