import baseStyled, { createGlobalStyle, ThemedStyledInterface, ThemeProvider } from 'styled-components'
import { normalize } from 'styled-normalize'

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap');
  ${normalize}
  * {
    font-family: 'Open Sans', sans-serif;
  },
`

export const lightTheme = {
  color: {
    lightAccent: '#FFFFFF',
    lightFaded: 'rgba(255,255,255,0.6)',
    lightShade: '#A8A8A8',
    content: '#707070',
    darkAccent: '#000000',
    darkShade: '#070707',
    darkFaded: '#1E1E1E',
    variant: '#707070',
    alternative: '#616161',
    primary: '#D81F26',
  },
  shadow: '0px 3px 16px 0px rgba(0,0,0,0.16)',
}

export const darkTheme = {
  color: {
    lightAccent: '#FFFFFF',
    lightFaded: 'rgba(255,255,255,0.6)',
    lightShade: '#A8A8A8',
    content: '#707070',
    darkAccent: '#000000',
    darkShade: '#070707',
    darkFaded: '#1E1E1E',
    variant: '#707070',
    alternative: '#616161',
    primary: '#D81F26',
  },
  shadow: '0px 3px 16px 0px rgba(0,0,0,0.16)',
}

export default GlobalStyle
export { ThemeProvider }
export type Theme = typeof lightTheme
export const styled = baseStyled as ThemedStyledInterface<Theme>
