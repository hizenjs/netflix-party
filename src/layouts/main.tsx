import React, { FC, ReactElement } from 'react'

const Main: FC = (): ReactElement => (
  <div>
    <h1>Main Layout</h1>
  </div>
)

export default Main
