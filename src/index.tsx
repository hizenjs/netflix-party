import React, { useContext } from 'react'
import ReactDOM from 'react-dom'
import GlobalStyle, { ThemeProvider, darkTheme } from './theme'
import { templates } from './components'
import AppProvider, { AppContext } from './provider'
import Placeholder from './assets/player.jpg'

const { Main } = templates

const App = () => {
  const { setShowPoll, showPoll } = useContext(AppContext)
  const doSomething = () => {
    setShowPoll(showPoll)
  }
  return (
    <Main
      header={{
        title: "Queen's gambit together party",
        subtitle: 'series - gueens gambit',
        actions: [
          { onPress: null, icon: 'share' },
          { onPress: null, icon: 'settings' },
          { onPress: null, icon: 'more' },
          { onPress: null, name: 'Finish the session' },
        ],
      }}
      video={{
        loading: false,
        maximized: false,
        source: Placeholder,
        stream: undefined,
        actions: [
          { onPress: null, icon: 'video' },
          { onPress: null, icon: 'cast' },
          { onPress: null, icon: 'rewind' },
          { onPress: null, icon: 'play' },
          { onPress: null, icon: 'forward' },
          { onPress: doSomething(), icon: 'chart' },
        ],
      }}
      chat={{
        tabs: [
          { onPress: null, name: 'Chat', active: true },
          { onPress: null, name: 'Episodes' },
        ],
        poll: {
          title: 'Did she will  eat another pills and  win the game?',
          options: [
            { name: 'yes', value: 72 },
            { name: 'no', value: 19 },
            { name: 'no opinion', value: 9 },
          ],
        },
        conversations: [
          {
            name: 'Elwin Sharvill',
            messages: ['Hey-hey!'],
            avatar: {
              type: 'circle',
              size: 'small',
              source: 'https://i.imgur.com/J2NsD3o.png',
            },
          },
          {
            name: 'Kita Chihoko',
            messages: ['Hi everyone:', 'Joseph here from California'],
            avatar: {
              type: 'circle',
              size: 'small',
              source: 'https://i.imgur.com/J2NsD3o.png',
            },
          },
          {
            name: 'Gvozden Boskovsky',
            messages: ['Hi everyone! Gvozden here from Canada'],
            avatar: {
              type: 'circle',
              size: 'small',
              source: 'https://i.imgur.com/J2NsD3o.png',
            },
          },
        ],
        compose: {
          placeholder: 'Write your message...',
          icons: ['mention', 'join', 'image'],
        },
      }}
    />
  )
}

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyle />
    <ThemeProvider theme={darkTheme}>
      <AppProvider>
        <App />
      </AppProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root'),
)
